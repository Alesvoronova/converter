<?php

if($argc == 3) {
    $arg = getopt("", ['from:', 'to:']);
    if (isset($arg['from']) && isset($arg['to'])) {
        $keys = array_keys($arg);
        if ($keys[0] != 'from') {
            echo "first argument must be --from\n";
        }
    } else {
        echo "must be --from and --to arguments\n";
    }
}else {
    echo "must be two arguments\n";
}

$from = $arg['from'];
$to = $arg['to'];
if(strtoupper($from) != $from || strtoupper($to) != $to){
    echo 'the argument value must be in uppercase';
}

$url = "https://www.nbrb.by/api/exrates/rates?periodicity=0";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$data = json_decode(curl_exec($ch), $assoc = true);
curl_close($ch);

if ($from == 'BYN') {
    $course_from = 1;
}

if ($to == 'BYN') {
    $course_to = 1;
}

$array_course = [];
foreach ($data as $course) {
    $array_course[] = $course['Cur_Abbreviation'];
}

if(!in_array($from, $array_course)){
    echo "the argument value is not correct\n";
}

if(!in_array($to, $array_course)){
    echo "the argument value is not correct\n";
}

foreach ($data as $course) {
    if ($course['Cur_Abbreviation'] == $from) {
        $course_from = $course['Cur_OfficialRate'];
    }
    if ($course['Cur_Abbreviation'] == $to) {
        $course_to = $course['Cur_OfficialRate'];
    }
}

if (isset($course_from) && isset($course_to)) {
    $result = round($course_from / $course_to, 3);
    echo $result . "\n";
}
